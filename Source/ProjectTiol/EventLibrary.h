// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EventLibrary.generated.h"

struct FMovieSceneEvent;
/**
 * 
 */
UCLASS()
class PROJECTTIOL_API UEventLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
